Google Translate Mac App
===================

Now run Google Translate on your Mac Desktop!

Requirements
-------------

 - 64bit Mac OS running 10.7 or later
 - Internet connection

Development
-------------

Want to contribute? Great!  

You can submit a pull request or simply share the project :)